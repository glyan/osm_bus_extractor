/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.osmBusExtractor

import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, SECONDS}
import com.tritcorp.transport.configuration.Conf._
import com.tritcorp.transport.osmBusExtractor.InterPoints.InterPoint
import com.tritcorp.transport.osmBusExtractor.Extraction._
import org.postgresql.util.PSQLException

private[osmBusExtractor] object Views {

  val ways_shapes: String =
    ways_shape_format match {
      case "kml" => "st_AsKml("
      case "geojson" => "st_asGeoJson("
      case "bin" => "st_asBinary("
      case "wkb" | "ewkb" => "st_asEwkb("
      case "wkt" | "ewkt" => "st_asEwkt("
      case "gml" => "st_asGml("
      case "hexwkb" => "st_asHexEwkb("
      case "svg" => "st_asSvg("
      case "text" => "st_asText("
      case "x3d" => "st_asX3d("
      case "geohash" => "st_Geohash("
      case _ => "st_asGeoJson("
    }


  def clear_all_temp_views() = {
    val drop = sql"""SELECT 'DROP VIEW ' || table_name || ' CASCADE;' as drop
                 FROM information_schema.views
                 WHERE table_schema NOT IN ('pg_catalog', 'information_schema')
                 AND table_name !~ '^pg_' AND table_name !~ '^ways_data_' AND table_name ~ '^ways_' OR table_name ~ '^nodes_';""".as[(String)]
    Await.result(db.run(drop), Duration(db_query_timeout, SECONDS)).foreach { drop_cmd =>
      Await.result(db.run(sqlu"""#$drop_cmd"""), Duration(db_query_timeout, SECONDS))
    }

  }

  def clear_temp_views(trip_osm_id: Long) = {
    val drop: DBIO[Int] = sqlu"""drop view if exists ways_#$trip_osm_id, nodes_#$trip_osm_id cascade"""
    Await.result(db.run(drop), Duration(db_query_timeout, SECONDS))
  }

  /**
   * Used to update temp views for each trip.
   *
   * @param trip_osm_id osm_id of the trip (bus line relation) to work on
   * @return
   */
  def update_temp_views(trip_osm_id: Long) = {

    clear_temp_views(trip_osm_id)

    /**
     * Will be used to identify the members within the OSM relation
     */
    val members_query = sql"""select members from planet_osm_rels where id='#${trip_osm_id}'""".as[String]
    val members = Await.result(db.run(members_query), Duration(db_query_timeout, SECONDS))

    /**
     * ar transforms the relation members's string into an array in which even values are osm_id starting with 'n' when nodes or 'w' when ways
     * odd values are meta data giving more information.
     * */
    val ar = members.toArray.head.replace("{", "").replace("}", "").split(",").zipWithIndex
    val ids = ar.filter(_._2 % 2 == 0)
    val meta = ar.filter(_._2 % 2 == 1)

    val stops_data = if (physicalBusStop)
      ids.zip(meta).filter(e => e._2._1.contains("platform") && e._1._1.contains("n")).map(_._1._1.replace("n", "")).zipWithIndex
    else
      ids.zip(meta).filter(e => e._2._1.contains("stop") && e._1._1.contains("n")).map(_._1._1.replace("n", "")).zipWithIndex

    val insert_stops = stops_data.map { s => (trip_osm_id, s._1.toLong, s._2) }

    /** Ensemble of inter-points extracted from OSM over the identified relations */
    val ips: Array[InterPoint] = stops_data.map { ip =>
      val next = stops_data.find(_._2 == ip._2 + 1).getOrElse(("-1", -1))
      InterPoint(trip_osm_id, ip._1, ip._2, next._1, next._2)
    }.filter(ip => ip.dest != "-1" && ip.order2 != -1)

    val insert = DBIO.seq(stops_table ++= insert_stops, ips_table ++= ips.map(ip => (trip_osm_id, ip.orig, ip.order1, ip.dest, ip.order2)))

    val result_stops = db.run(insert)
    Await.result(result_stops, Duration(db_query_timeout, SECONDS))


    /**
     * View containing all the ways of the trip identified by trip_osm_id
     */
    val ways: DBIO[Int] =
      sqlu"""create or replace view ways_#$trip_osm_id as (select * from planet_osm_line where osm_id in
            (select unnest(parts) as osm_id from planet_osm_rels where id='#$trip_osm_id'))"""


    val publicTransportTag: String = if (physicalBusStop) "platform" else "stop_position"
    /**
     * View containing all the nodes of the trip identified by trip_osm_id that are identified as bus stops of the KR network
     */
    val nodes: DBIO[Int] = if (use_ref_key_stops) {
      sqlu"""create or replace view nodes_#$trip_osm_id as (select * from planet_osm_point where
            tags->'#$ref_key_stops' is not null and tags->'public_transport'='#$publicTransportTag' and osm_id in
            (select stop_osm_id as osm_id from bus_stops where rel_osm_id='#$trip_osm_id'))"""
    }
    else {
      sqlu"""create or replace view nodes_#$trip_osm_id as (select * from planet_osm_point where
            tags->'public_transport'='#$publicTransportTag' and osm_id in
            (select stop_osm_id as osm_id from bus_stops where rel_osm_id='#$trip_osm_id'))"""
    }

    /**
     * Distances in meters between each node and each way of the trip identified by trip_osm_id
     */
    val distances: DBIO[Int] =
      sqlu"""create or replace view distances_#$trip_osm_id as (select distinct ways_#$trip_osm_id.name as way_name, ways_#$trip_osm_id.osm_id as way_id,
            nodes_#$trip_osm_id.name as node_name, nodes_#$trip_osm_id.osm_id as node_id, nodes_#$trip_osm_id.tags->'#$ref_key_stops' as stop_id,
            nodes_#$trip_osm_id.way as node_way,st_distance(st_transform(nodes_#$trip_osm_id.way,#$epsg),st_transform(ways_#$trip_osm_id.way,#$epsg))
            as dist from ways_#$trip_osm_id,nodes_#$trip_osm_id order by node_id, dist)"""

    /**
     * Nearest nodes<=>ways within the trip identified by trip_osm_id
     */
    val way_node: DBIO[Int] =
      sqlu"""create or replace view way_node_#$trip_osm_id as (select distances_#$trip_osm_id.way_name, distances_#$trip_osm_id.way_id, distances_#$trip_osm_id.node_name,
            distances_#$trip_osm_id.node_id, distances_#$trip_osm_id.stop_id, distances_#$trip_osm_id.node_way, distances_#$trip_osm_id.dist from distances_#$trip_osm_id,
            (select node_id, min(dist) as dist from distances_#$trip_osm_id group by node_id order by dist) as d where
            d.node_id=distances_#$trip_osm_id.node_id and d.dist=distances_#$trip_osm_id.dist)"""

    /**
     * distances between ways and nodes that are not the smallest distances for each pair (node,way) (i.e not in way_node)
     * within the trip identified by trip_osm_id
     */
    val distances2: DBIO[Int] =
      sqlu"""create or replace view distances2_#$trip_osm_id as (select * from distances_#$trip_osm_id except (select * from way_node_#$trip_osm_id))"""

    /**
     * nodes<=>ways distances that are not the smallest (i.e not in way_node) with a distance smaller than nearestRadiusMeter,
     * within the trip identified by trip_osm_id
     */
    val way_node2: DBIO[Int] =
      sqlu"""create or replace view way_node2_#$trip_osm_id as (select distances2_#$trip_osm_id.way_name, distances2_#$trip_osm_id.way_id, distances2_#$trip_osm_id.node_name,
            distances2_#$trip_osm_id.node_id, distances2_#$trip_osm_id.stop_id, distances2_#$trip_osm_id.node_way, distances2_#$trip_osm_id.dist from distances2_#$trip_osm_id,
            (select node_id, min(dist) as dist from distances2_#$trip_osm_id group by node_id order by dist) as d where
            d.node_id = distances2_#$trip_osm_id.node_id and d.dist = distances2_#$trip_osm_id.dist and distances2_#$trip_osm_id.dist < '#$nearestRadiusMeter') order by node_id, dist"""

    /**
     * Union of way_node and way_node2, TEMPORARY
     */
    val stops_ways_temp: DBIO[Int] =
      sqlu"""create or replace view stops_ways_temp_#$trip_osm_id as (select * from way_node_#$trip_osm_id union (select * from way_node2_#$trip_osm_id))"""

    /**
     * List of bus stops and their nearest ways (around 20 meters radius) within the trip identified by trip_osm_id
     */
    val stops_ways: DBIO[Int] =
      sqlu"""create or replace view stops_ways_#$trip_osm_id as (select way_name,way_id,node_name,node_id,node_way,stop_id,dist,way from
            stops_ways_temp_#$trip_osm_id,ways_#$trip_osm_id where way_id=osm_id order by node_id, dist)"""

    /**
     * Number of neighbouring ways per bus stop
     */
    val neighbours_per_stop: DBIO[Int] =
      sqlu"""create or replace view neighbours_per_stop_#$trip_osm_id as (select node_id, count(*) as nb_neigh_ways from stops_ways_#$trip_osm_id group by node_id)"""

    val way_points: DBIO[Int] =
      sqlu"""create or replace view way_points_#$trip_osm_id as (select distinct ways_#$trip_osm_id.osm_id as way_id, p.osm_id,
            p.tags->'highway' as type_highway, p.tags->'level_crossing' as lvl_crossing from planet_osm_point p,
            ways_#$trip_osm_id where  st_intersects(p.way, ways_#$trip_osm_id.way) and (p.tags->'highway'='give_way' or p.tags->'highway'='stop' or
            p.tags->'highway'='crossing' or p.tags->'highway'='traffic_signals' or p.tags->'level_crossing'='automatic_barrier'))"""

    val traffic_signals: DBIO[Int] =
      sqlu"""create or replace view traffic_signals_#$trip_osm_id as (select way_id as tway_id, count(*) as nb_traffic_signals from
             way_points_#$trip_osm_id where type_highway='traffic_signals' group by way_id)"""

    val crossings: DBIO[Int] =
      sqlu"""create or replace view crossings_#$trip_osm_id as (select way_id as cway_id, count(*) as nb_crossings from
            way_points_#$trip_osm_id where type_highway='crossing' group by way_id)"""

    val stops: DBIO[Int] =
      sqlu"""create or replace view stops_#$trip_osm_id as (select way_id as sway_id, count(*) as nb_stops from way_points_#$trip_osm_id where type_highway='stop' group by way_id)"""

    val give_ways: DBIO[Int] =
      sqlu"""create or replace view give_ways_#$trip_osm_id as (select way_id as gway_id, count(*) as nb_give_ways from way_points_#$trip_osm_id where type_highway='give_way' group by way_id)"""

    val lvl_crossings: DBIO[Int] =
      sqlu"""create or replace view lvl_crossings_#$trip_osm_id as (select way_id as lway_id, count(*) as nb_lvl_crossings from way_points_#$trip_osm_id where lvl_crossing='automatic_barrier' group by way_id)"""

    val roundabouts: DBIO[Int] =
      sqlu"""create or replace view roundabouts_#$trip_osm_id as (select distinct osm_id as rway_id, 1 as roundabout from ways_#$trip_osm_id where tags->'junction'='roundabout')"""

    val ways_data: DBIO[Int] =
      sqlu"""create or replace view ways_data_#$trip_osm_id as(
            select w.osm_id as way_id, #$ways_shapes w.way) as way, w.tags->'name' as name, w.tags->'lanes' as nb_lanes, w.tags->'maxspeed' as legal_speed,
            w.tags->'zone:maxspeed' as speed_zone, w.tags->'oneway' as oneway,
            CASE
            WHEN w.tags->'cycleway' is null and w.tags->'cycleway:right' is null and w.tags->'cycleway:left' is null and w.tags->'cycleway:both' is null and w.tags->'bicycle' is null THEN 'true'
            WHEN w.tags->'access' = 'no' or w.tags->'access' = 'discouraged' or w.tags->'bicycle' = 'no' or w.tags->'bicycle' = 'discouraged' or w.tags->'cycleway'='track' or w.tags->'cycleway'='lane' or w.tags->'cycleway'='opposite_lane' or w.tags->'cycleway'='right' or
            w.tags->'cycleway'='left' or w.tags->'cycleway'='segregated' or w.tags->'cycleway'='both' or w.tags->'cycleway'='sidewalk' or w.tags->'cycleway:right'='yes' or
            w.tags->'cycleway:right'='track' or w.tags->'cycleway:right'='lane' or w.tags->'cycleway:right'='opposite_track' or w.tags->'cycleway:right'='use_sidepath' or
            w.tags->'cycleway:left'='yes' or w.tags->'cycleway:left'='track' or w.tags->'cycleway:left'='lane' or w.tags->'cycleway:left'='opposite_lane' or
            w.tags->'cycleway:left'='opposite_track' or w.tags->'cycleway:both'='track' or w.tags->'cycleway:both'='lane' THEN 'false'
            ELSE 'true'
            END
            AS bike,
            CASE
            WHEN w.tags->'cycleway'='asl' THEN 'true'
            ELSE 'false'
            END
            AS bike_box,
            CASE
            WHEN w.tags->'busway'='yes' or w.tags->'busway'='track' or w.tags->'busway'='lane' or w.tags->'service'='busway' or w.tags->'bus'='designated' or
            w.tags->'bus'='dedicated' or w.tags->'bus'='lane' or w.tags->'bus'='yes' or w.tags->'busway:right'='lane' or w.tags->'busway:left'='lane' or
            w.tags->'busway:left'='opposite_lane' THEN 'true'
            ELSE 'false'
            END
            AS bus_lane,
            CASE
            WHEN w.tags->'bus'='dedicated' THEN 'true'
            ELSE 'false'
            END
            AS dedicated_bus_lane,
            ceil(st_length(st_transform(w.way, #$epsg))) as length, nb_traffic_signals, nb_crossings, nb_stops, nb_give_ways, nb_lvl_crossings, roundabout
            from ways_#$trip_osm_id w left outer join traffic_signals_#$trip_osm_id t on tway_id=w.osm_id
            left outer join crossings_#$trip_osm_id c on cway_id=w.osm_id
            left outer join stops_#$trip_osm_id s on sway_id=w.osm_id
            left outer join give_ways_#$trip_osm_id g on gway_id=w.osm_id
            left outer join lvl_crossings_#$trip_osm_id l on lway_id=w.osm_id
            left outer join roundabouts_#$trip_osm_id r on rway_id=w.osm_id
          )"""

    Await.result(db.run(ways >> nodes >> distances >> way_node >> distances2 >> way_node2 >> stops_ways_temp >> stops_ways >>
      neighbours_per_stop >> way_points >> traffic_signals >> crossings >> stops >> give_ways >> lvl_crossings >> roundabouts >> ways_data), Duration(db_query_timeout, SECONDS))

    ips
  }

}
