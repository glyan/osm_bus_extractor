/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.osmBusExtractor

import slick.lifted.{ForeignKeyQuery, ProvenShape}
import slick.jdbc.PostgresProfile.api._

private[osmBusExtractor] class BusStops(tag: Tag)
  extends Table[(Long, Long, Int)](tag, "bus_stops") {

  def rel_osm_id: Rep[Long] = column[Long]("rel_osm_id")

  def stop_osm_id: Rep[Long] = column[Long]("stop_osm_id")

  def idx: Rep[Int] = column[Int]("idx")

  def * : ProvenShape[(Long, Long, Int)] =
    (rel_osm_id, stop_osm_id, idx)

}

object BusStops {

  case class BusStops(rel_osm_id: Long, stop_osm_id: Long, idx: Int)

}