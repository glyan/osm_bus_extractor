/*
 * Copyright (c) 2020.  Gauthier LYAN
 */

package com.tritcorp.transport.osmBusExtractor

import com.tritcorp.mt4s.scalaTest.FlatSpecTest

class RunThroughScalaTest extends FlatSpecTest{

  "The software" should "run" in {
    Extraction.main(Array());
  }

}
