# OpenStreetMaps Bus Lines Metadata Extractor

This software was built in order to answer the question "What are the infrastructures present over the roads sections that a bus line travels trough ?" 
It uses OSM2PGSQL as a data source in order to gather metadata for bus lines over any urban bus network.

In this current version, we gather information for each way between all the bus stops of any identified bus line such as :

 - speed zone
 - legal speed
 - bikes on road / apart
 - bus lane
 - length of ways
 - shape of ways (as LineString objects)
 - roundabouts (wether a way is in a roundabout or no)
 - number of crossings
 - number of traffic signals
 - number of level crossings
 - number of stops
 - number of giveways
 - misc. metadata
 
 
 ### System Dependencies
 
 - Java JDK 1.8 (Mandatory)
 - Osm2pgsql (Mandatory)
 - Apache Spark 2.4.5 (Optional)
 - SBT-Scala Build Tool (Optional)
 - MultipleTest4Spark : https://github.com/Tritbool/MultipleTest4Spark (Mandatory)
  
 ### Preparing the environment
 
 1. Install osm2pgsql and all its dependencies on any system that will be reachable for the software (i.e osm2pgsql can be deployed on any hardware as long as the machine that runs osm_bus_extractor can reach it)
 
 2. Get the data extract you need on https://protomaps.com/extracts
 Getting a specific area implies loading a smaller file, hence everything should be faster then getting a whole country or more.
 
 3. Create the postgres database with hstore and postgis extensions then populate it using osm2pgsql with those **mandatories options**`osm2pgsql --slim  --latlong --create --hstore-all --multi-geometry --database <target database> planet.osm.pbf`
 Refer to osm2pgsql documentation for other non-mandatory options such as ` --number-processes <num processes>`. 
  
 ### Configuring the software 
  
  The configuration is done through the `application.conf` File.
  It is made to :
  - Tweak Spark (either locally or distributed even if distributed is not necessary in a first place)
  - Configure the Postgres database
  - Configure the software behavior (Please see application.conf comments)
  **N.B** Bus lines (or trips) are automatically identified using networks and/or operators tags
  
  
 ### Run the software
 
  **This software will probably not run in spark cluster mode due to the interconnectivity with postgresql using slick** 
 
 1) The easy way is to use IntelliJ Idea with SBT plugin installed and run it directly in the IDE. This method is a good workaround if you don't want to deploy a JDK and Spark on your system
          
    ***N.B*** This will work on Linux systems only as long as windows needs a different Spark / Hadoop configuration to work (Cf Hadoop Winutils)
 
 2) Run the test class `RunThroughScalaTest` that is another workaround for linux, which allows to run the software through SBT without installing Apache Spark.
 Beware that using this method `src/test/resources/application.conf` will be used instead of `src/main/resources/application.conf`
 
    **N.B** The easy way to kill the app through this method is to get the PID through jps : `jps | grep sbt-launch.jar | sed s/" sbt-launch.jar"//g`
    
  
 
 # How it works
 
 It is working on almost any sufficiently referenced bus network over OSM.
 The way it identifies bus lines and bus stops order is based on the OSM 
 documentation that states that they are ordered in the way the vehicles travel
 along them https://wiki.openstreetmap.org/wiki/Relation
 
 Bus routes are relations which mean they are objects that contains ways and/or nodes.
 **N.B** : A bus route that does not contain any node will not be processed by the software because the nodes are mandatory to iterate through osm2pgsql's database.
 
Example of a valid bus line relation
---
![sample_line](img/bus_line.png) 

Example of a valid bus stop
---
![sample_line](img/bus_stop.png)

# Output example : Dublin
 
```
+---------+--------------------+-----+---------------+--------+-----------+-------------+------+-----------+--------+--------+------------------+------+------------------+------------+--------+------------+----------------+----------+------------+------------+------------+------------+------------+------------+------------+------------+-----------------------------+----------+----------+-------+--------------------+------+----------+---+----------+----------------+
|   way_id|               shape|index|           name|nb_lanes|legal_speed|speed_zone_30|oneway|shares_bike|bike_box|bus_lane|dedicated_bus_lane|length|nb_traffic_signals|nb_crossings|nb_stops|nb_give_ways|nb_lvl_crossings|roundabout|stop_id_orig|node_id_orig|coord_x_orig|coord_y_orig|stop_id_dest|node_id_dest|coord_x_dest|coord_y_dest|nb_neighbouring_ways_bus_stop|order_orig|order_dest|id_line|           line_name|colour|  operator|ref|      from|              to|
+---------+--------------------+-----+---------------+--------+-----------+-------------+------+-----------+--------+--------+------------------+------+------------------+------------+--------+------------+----------------+----------+------------+------------+------------+------------+------------+------------+------------+------------+-----------------------------+----------+----------+-------+--------------------+------+----------+---+----------+----------------+
|682960616|{"type":"LineStri...|    1|Harristown Road|       1|         50|        false|  true|       true|   false|   false|             false|  46.0|                 0|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            1|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|683377937|{"type":"LineStri...|    2|           null|       1|         50|        false| false|       true|   false|   false|             false|  33.0|                 0|           0|       0|           0|               0|      true|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
| 26443154|{"type":"LineStri...|    3|Harristown Road|       1|         50|        false| false|       true|   false|   false|             false| 844.0|                 1|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|683377935|{"type":"LineStri...|    4|      Naul Road|       2|         60|        false| false|       true|   false|   false|             false|  41.0|                 1|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|109416729|{"type":"LineStri...|    5|      Naul Road|       2|         60|        false| false|       true|   false|   false|             false| 550.0|                 0|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|109416732|{"type":"LineStri...|    6|      Naul Road|       2|         60|        false|  true|       true|   false|   false|             false| 242.0|                 2|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|512903045|{"type":"LineStri...|    7|           null|       2|         50|        false| false|       true|   false|   false|             false|  47.0|                 1|           0|       0|           0|               0|      true|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
| 98479820|{"type":"LineStri...|    8|           null|       2|         50|        false| false|       true|   false|   false|             false|  71.0|                 0|           0|       0|           0|               0|      true|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
| 98479835|{"type":"LineStri...|    9|           null|       2|         50|        false| false|       true|   false|   false|             false|  32.0|                 1|           0|       0|           0|               0|      true|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|512903044|{"type":"LineStri...|   10|           null|       2|         50|        false| false|       true|   false|   false|             false|  13.0|                 1|           0|       0|           0|               0|      true|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|  3786766|{"type":"LineStri...|   11|      Naul Road|       2|         60|        false|  true|      false|   false|   false|             false| 358.0|                 1|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|667790294|{"type":"LineStri...|   12|      Naul Road|       2|         60|        false|  true|      false|   false|   false|             false| 157.0|                 2|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|  3786767|{"type":"LineStri...|   13|      Naul Road|       2|         60|        false|  true|      false|   false|   false|             false| 315.0|                 2|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|116264278|{"type":"LineStri...|   14|  Ballymun Road|       3|         50|        false|  true|       true|   false|   false|             false|   5.0|                 1|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|335758260|{"type":"LineStri...|   15|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false| 131.0|                 0|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
| 54637720|{"type":"LineStri...|   16|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false| 369.0|                 0|           2|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|121294135|{"type":"LineStri...|   17|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false| 265.0|                 1|           3|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|667361179|{"type":"LineStri...|   18|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false|  37.0|                 1|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|136458035|{"type":"LineStri...|   19|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false| 243.0|                 0|           1|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
|317003245|{"type":"LineStri...|   20|  Ballymun Road|       3|         50|        false|  true|      false|   false|   false|             false| 212.0|                 0|           0|       0|           0|               0|     false|         324|  3153341302|  -6.2786561|   53.417714|          39|  1496722633|  -6.2656675|  53.3804288|                            0|         0|         1|2709367|4 - Harristown to...|  null|Dublin Bus|  4|Harristown|Monkstown Avenue|
+---------+--------------------+-----+---------------+--------+-----------+-------------+------+-----------+--------+--------+------------------+------+------------------+------------+--------+------------+----------------+----------+------------+------------+------------+------------+------------+------------+------------+------------+-----------------------------+----------+----------+-------+--------------------+------+----------+---+----------+----------------+
only showing top 20 rows

```
 The output is ordered by inter-points which consists of the set of ways between two contiguous bus stops (Columns order_orig, order_dest). Also, the ways of each inter-point are ordered in the way the bus should go through them in reality (column index) 
 
 default delimiter is semicolon because some fields in OSM can contain commas
 
 Dublin actual network vs extracted data (generated via GeoSpark, using the shape column of each trip)
 ---
 ![sample_line](img/dublin.png)
 ![sample_line](img/dublin_gen.png)
 ---
 
 This example is runnable using the osm extract name `dublin.osm.pbf` present in `examples/Dublin` using ["STM","STL","EXO"] as operators list and ["Dublin Bus","Transport for Ireland"] as networks list,
 `ref_key_stops="ref"` and `network_or_operator=2`
 The extraction should take between 4-6 hours for 62 bus lines on a 4 cores (8 threads) laptop
 
 Other outputs examples for a few cities over the world are available in `examples/outputs` 
 
 
# limitations  
 - path finding is returning the smallest path. In some cases it might return an incomplete path (when the bus takes a one way loop for exemple)
 ![sample_line](img/loop_example.png)
 - There might be data redundancy as long as two different trips that travels through the same roads will yield similar/identical data.
 
 - The path finding is theoretically not supposed to fall in an infinite loop, yet there may be rare cases in which it won't end. **In this case if one can identify which relation yields an infinite loop please open an issue with the relation references in order to try to enhance the path finding system**.
 
 - When a non-terminal bus stop is located in the somewhere along a road, we arbitrarily split the road's length in two for the current and its neighbouring inter-point.
    However, we do keep all the traffic signals, crossing, etc bound to this road in both of the inter points.
    To reduce this unwanted noise, an evolution would be to split the roads according to the actual bus stop's position and try to keep the infrastructure elements that are matching the split.
 
 - This software uses Apache Spark in order to facilitate the gathering of data and writing. However, it should not be run on another mode than LOCAL MODE unless all the nodes cluster on which the application
 will be deployed has an access to the postgres database on which osm2pgsql's databases are located.
 
 **N.B** Cluster mode has not been tested, and the software was not built for this purpose so its behavior in cluster mode is uncertain. 
  
---