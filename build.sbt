name := "osm_bus_extractor"
 
resolvers += Resolver.mavenLocal

version := "1.0"

scalaVersion := "2.11.12"

scalacOptions += "-target:jvm-1.8"

organization := "com.tritcorp.transport"

organizationName := "Gauthier LYAN"

  libraryDependencies ++= Seq(
    "com.tritcorp.exp" %% "mt4s" % "1.3.4" % "test",
    "org.apache.spark" %% "spark-core" % "2.4.5",
    "org.apache.spark" %% "spark-sql" % "2.4.5",
    "org.apache.spark" %% "spark-hive" % "2.4.5",
    "org.apache.spark" %% "spark-mllib" % "2.4.5",
    "org.datasyslab" % "geospark" % "1.3.1",
    "org.datasyslab" % "geospark-sql_2.3" % "1.3.1",
    "org.datasyslab" % "geospark-viz_2.3" % "1.3.1",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
    "com.typesafe" % "config" % "1.4.0",
    "com.github.haifengl" %% "smile-scala" % "2.4.0",
    "com.typesafe.slick" %% "slick" % "3.3.1",
    //"org.slf4j" % "slf4j-nop" % "1.7.26",
    "com.typesafe.slick" %% "slick-hikaricp" % "3.3.1",
    "org.postgresql" % "postgresql" % "42.2.10"
  )
  
  test in assembly := {}

  assemblyMergeStrategy in assembly := {
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case _ => MergeStrategy.first
  }
